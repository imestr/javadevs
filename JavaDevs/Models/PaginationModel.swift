//
//  PaginationModel.swift
//  JavaDevs
//
//  Created by Klemen Košir on 05/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import Foundation

class PaginationModel<T: Decodable>: Decodable {
    private(set) var items: [T]
    private(set) var startCursor: String?
    private(set) var endCursor: String?
    private(set) var hasNextPage: Bool
    // total count for users, if page contains user
    private(set) var userCount: Int?
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    private enum DataCodingKeys: String, CodingKey {
        case search
    }
    
    private enum SearchCodingKeys: String, CodingKey {
        case nodes
        case pageInfo
        case userCount
    }
    
    private enum PageInfoCodingKeys: String, CodingKey {
        case hasNextPage
        case startCursor
        case endCursor
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: DataCodingKeys.self, forKey: .data)
        let searchContainer = try dataContainer.nestedContainer(keyedBy: SearchCodingKeys.self, forKey: .search)
        items = try searchContainer.decode([T].self, forKey: .nodes)
        
        let pageInfoContainer = try searchContainer.nestedContainer(keyedBy: PageInfoCodingKeys.self, forKey: .pageInfo)
        hasNextPage = try pageInfoContainer.decode(Bool.self, forKey: .hasNextPage)
        startCursor = try pageInfoContainer.decode(String?.self, forKey: .startCursor)
        endCursor = try pageInfoContainer.decode(String?.self, forKey: .endCursor)
        
        userCount = try? searchContainer.decode(Int.self, forKey: .userCount)
    }
    
    func addNextPage(_ pagination: PaginationModel<T>) {
        self.items += pagination.items
        self.endCursor = pagination.endCursor
        self.hasNextPage = pagination.hasNextPage
        self.userCount = pagination.userCount
    }
    
}
