//
//  UserResponseModel.swift
//  JavaDevs
//
//  Created by Klemen Košir on 05/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import Foundation

struct UserResponseModel: Decodable {
    
    let user: User
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    private enum DataCodingKeys: String, CodingKey {
        case user
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: DataCodingKeys.self, forKey: .data)
        user = try dataContainer.decode(User.self, forKey: .user)
    }
    
}
