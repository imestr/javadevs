//
//  User.swift
//  JavaDevs
//
//  Created by Klemen Košir on 05/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import Foundation

struct User: Decodable {
    
    let id: String
    let username: String
    let name: String?
    let email: String?
    let avatarUrl: URL?
    let createdAt: Date
    let numOfFollowers: Int?
    let locationName: String?
    let profileUrl: URL?
    
    var formattedCreatedAt: String {
        let df = DateFormatter()
        df.dateFormat = "d. M. yyyy"
        return df.string(from: createdAt)
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case username = "login"
        case name
        case email
        case avatarUrl
        case createdAt
        case followers
        case location
        case profileUrl = "url"
    }
    
    private enum FollowersCodingKeys: String, CodingKey {
        case totalCount
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        username = try container.decode(String.self, forKey: .username)
        name = try container.decode(String?.self, forKey: .name)
        email = try container.decode(String?.self, forKey: .email)
        avatarUrl = try container.decode(URL?.self, forKey: .avatarUrl)
        let createdAtString = try container.decode(String.self, forKey: .createdAt)
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let d = df.date(from: createdAtString) {
            createdAt = d
        }
        else {
            throw DecodingError.dataCorrupted(.init(codingPath: [CodingKeys.createdAt], debugDescription: "Couldn't parse date."))
        }
        locationName = try container.decodeIfPresent(String.self, forKey: .location)
        profileUrl = try container.decodeIfPresent(URL.self, forKey: .profileUrl)
        let followersContainer = try? container.nestedContainer(keyedBy: FollowersCodingKeys.self, forKey: .followers)
        numOfFollowers = try followersContainer?.decode(Int.self, forKey: .totalCount)
    }
}

extension User {
    
    static func getUsers(language: String, startCursor: String? = nil, completion: @escaping NetworkCompletion<PaginationModel<User>>) {
        ApiManager.shared.makeRequest(networkRequest: NetworkRequest.getUsers(language: language, startCursor: startCursor), completion: completion)
    }
    
    static func getUser(username: String, completion: @escaping NetworkCompletion<UserResponseModel>) {
        ApiManager.shared.makeRequest(networkRequest: NetworkRequest.getUser(username: username), completion: completion)
    }
    
}
