//
//  ApiManager.swift
//  JavaDevs
//
//  Created by Klemen Košir on 04/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import Foundation

typealias NetworkCompletion<T: Decodable> = (_ result: NetworkResult<T>) -> Void

enum NetworkResult<T: Decodable> {
    case success(T)
    case failure(Error)
}

enum NetworkRequest {
    case getUsers(language: String, startCursor: String?)
    case getUser(username: String)
    
    private static let token = "c2183ca4686b98a04f80189b1d325d1dc45022ba"
    
    var headers: [String : String]? {
        var fields = [String : String]()
        if self.requiresAuthorization {
            fields["Authorization"] = "Bearer \(NetworkRequest.token)"
        }
        switch self {
        case .getUsers:
            return fields
        case .getUser:
            return fields
        }
    }
    
    var body: Data? {
        return try? JSONEncoder().encode(["query": graphQL])
    }
    
    var graphQL: String? {
        switch self {
        case .getUsers(let language, let startCursor):
            let graphQL = loadGraphQLString(from: "GetUsers")
            let sc = startCursor != nil ? "\"\(startCursor!)\"" : "null"
            return graphQL?.replacingOccurrences(of: "#lang#", with: language).replacingOccurrences(of: "#startCursor#", with: sc)
        case .getUser(let username):
            let graphQL = loadGraphQLString(from: "GetUser")
            return graphQL?.replacingOccurrences(of: "#username#", with: username)
        }
    }
    
    var url: URL {
        return URL(string: urlString)!
    }
    
    var urlString: String {
        return "https://api.github.com/graphql"
    }
    
    var requiresAuthorization: Bool {
        return true // currently all endpoints require authorization
    }
    
    private func loadGraphQLString(from filename: String) -> String? {
        guard let fileURL = Bundle.main.url(forResource: filename, withExtension: nil),
            let fileString = try? String(contentsOf: fileURL) else {
                return nil
        }
        return fileString
    }
    
}

class ApiManager {
    
    static let shared = ApiManager()
    
    private init() {}
    
    func makeRequest<T: Decodable>(networkRequest: NetworkRequest, completion: @escaping NetworkCompletion<T>) {
        
        let session = URLSession.shared
        
        var request = URLRequest(url: networkRequest.url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = networkRequest.headers
        request.httpBody = networkRequest.body
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let d = data {
                do {
                    let decodedData = try JSONDecoder().decode(T.self, from: d)
                    DispatchQueue.main.async {
                        completion(.success(decodedData))
                    }
                }
                catch let err {
                    DispatchQueue.main.async {
                        completion(.failure(err))
                    }
                }
            }
            else if let err = error {
                DispatchQueue.main.async {
                    completion(.failure(err))
                }
            }
        }
        
        task.resume()
    }
    
}
