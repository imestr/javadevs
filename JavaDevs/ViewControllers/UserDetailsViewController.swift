//
//  UserDetailsViewController.swift
//  JavaDevs
//
//  Created by Klemen Košir on 05/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController {
    
    static func viewController() -> UserDetailsViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userDetailsViewController") as! UserDetailsViewController
    }
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var joinedLabel: UILabel!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var profileUrlLabel: UILabel!
    @IBOutlet private weak var numOfFollowersLabel: UILabel!
    @IBOutlet private weak var nameTitleLabel: UILabel!
    @IBOutlet private weak var usernameTitleLabel: UILabel!
    @IBOutlet private weak var emailTitleLabel: UILabel!
    @IBOutlet private weak var joinedTitleLabel: UILabel!
    @IBOutlet private weak var locationTitleLabel: UILabel!
    @IBOutlet private weak var profileUrlTitleLabel: UILabel!
    @IBOutlet private weak var numOfFollowersTitleLabel: UILabel!
    
    var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Configuration.backgroundColor
        
        resetUI()
        showPlaceholders()
        
        loadFullUser()
    }
    
    private func loadFullUser() {
        User.getUser(username: user.username) { [weak self] (result) in
            switch result {
            case .success(let userResponse):
                self?.user = userResponse.user
                self?.showUser()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func showUser() {
        resetUI()
        
        nameLabel.text = user.name
        usernameLabel.text = user.username
        emailLabel.text = user.email
        joinedLabel.text = user.formattedCreatedAt
        locationLabel.text = user.locationName
        profileUrlLabel.text = user.profileUrl?.absoluteString
        numOfFollowersLabel.text = "\(user.numOfFollowers ?? 0)"
        
        avatarImageView.loadImage(url: user.avatarUrl)
        
    }

    private func resetUI() {
        nameLabel.text = nil
        usernameLabel.text = nil
        emailLabel.text = nil
        joinedLabel.text = nil
        locationLabel.text = nil
        profileUrlLabel.text = nil
        numOfFollowersLabel.text = nil
        avatarImageView.image = nil
        
        nameLabel.backgroundColor = .clear
        usernameLabel.backgroundColor = .clear
        emailLabel.backgroundColor = .clear
        joinedLabel.backgroundColor = .clear
        locationLabel.backgroundColor = .clear
        profileUrlLabel.backgroundColor = .clear
        numOfFollowersLabel.backgroundColor = .clear
        avatarImageView.backgroundColor = .clear
        
        let textColor = Configuration.textColor
        nameLabel.textColor = textColor
        usernameLabel.textColor = textColor
        emailLabel.textColor = textColor
        joinedLabel.textColor = textColor
        locationLabel.textColor = textColor
        profileUrlLabel.textColor = textColor
        numOfFollowersLabel.textColor = textColor
        
        let titleTextColor = textColor.withAlphaComponent(0.5)
        nameTitleLabel.textColor = titleTextColor
        usernameTitleLabel.textColor = titleTextColor
        emailTitleLabel.textColor = titleTextColor
        joinedTitleLabel.textColor = titleTextColor
        locationTitleLabel.textColor = titleTextColor
        profileUrlTitleLabel.textColor = titleTextColor
        numOfFollowersTitleLabel.textColor = titleTextColor
    }
    
    private func showPlaceholders() {
        let placeholderColor = UIColor(white: 0.97, alpha: 1.0)
        nameLabel.backgroundColor = placeholderColor
        usernameLabel.backgroundColor = placeholderColor
        emailLabel.backgroundColor = placeholderColor
        joinedLabel.backgroundColor = placeholderColor
        locationLabel.backgroundColor = placeholderColor
        profileUrlLabel.backgroundColor = placeholderColor
        numOfFollowersLabel.backgroundColor = placeholderColor
        avatarImageView.backgroundColor = placeholderColor
    }
    
    @IBAction func emailTapGesture(_ sender: Any) {
        guard let email = user.email,
            let url = URL(string: "mailto:\(email)") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func profileUrlTapGesture(_ sender: Any) {
        guard let profileUrl = user.profileUrl else { return }
        UIApplication.shared.open(profileUrl, options: [:], completionHandler: nil)
    }
    
}
