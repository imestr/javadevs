//
//  ViewController.swift
//  JavaDevs
//
//  Created by Klemen Košir on 04/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var pagination: PaginationModel<User>?
    
    var items: [User]? {
        return pagination?.items
    }

    private var lastEndCursor: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Configuration.backgroundColor
        
        loadUsersPage(initialLoad: true)
    }
    
    private func loadUsersPage(initialLoad: Bool = false) {
        if !initialLoad, lastEndCursor == pagination?.endCursor {
            return
        }
        lastEndCursor = pagination?.endCursor
        User.getUsers(language: "java", startCursor: pagination?.endCursor, completion: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let paginationObject):
                if let p = strongSelf.pagination {
                    p.addNextPage(paginationObject)
                }
                else {
                    strongSelf.pagination = paginationObject
                }
                strongSelf.tableView.reloadData()
                strongSelf.saveLoadedUserNames()
            case .failure(let error):
                print(error)
            }
        })
    }
    
    private func saveLoadedUserNames() {
        guard let users = pagination?.items else { return }
        let loadedUsersNames = users.compactMap({ $0.name })
        UserDefaults(suiteName: "group.klemenkosir.JavaDevs")?.set(loadedUsersNames, forKey: "loadedUsers")
    }


}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (pagination?.items.count ?? 4) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserTableViewCell
        var user: User? = nil
        if items?.count ?? 0 > indexPath.row {
            user = items![indexPath.row]
        }
        else {
            loadUsersPage()
        }
        cell.set(user: user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = items![indexPath.row]
        
        let userDetailsVC = UserDetailsViewController.viewController()
        userDetailsVC.user = user
        self.navigationController?.pushViewController(userDetailsVC, animated: true)
    }
    
}

