//
//  UIImageViewExtension.swift
//  JavaDevs
//
//  Created by Klemen Košir on 05/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import UIKit

extension UIImageView {
    
    private static let imageLoadingSession = URLSession(configuration: .default)
    private static let imageCache = NSCache<AnyObject, UIImage>()
    
    func loadImage(url: URL?) {
        guard let url = url else {
            return
        }
        
        if let image = UIImageView.imageCache.object(forKey: url as AnyObject) {
            self.image = image
            return
        }
        
        let session = UIImageView.imageLoadingSession
        session.dataTask(with: url) { [weak self] (data, response, error) in
            guard let d = data,
                let image = UIImage(data: d) else { return }
            if let responseUrl = response?.url {
                UIImageView.imageCache.setObject(image, forKey: responseUrl as AnyObject)
                if url == responseUrl {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }.resume()
        
    }
    
}
