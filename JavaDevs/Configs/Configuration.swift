//
//  Configuration.swift
//  JavaDevs
//
//  Created by Klemen Košir on 06/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import UIKit

enum Configuration {
    
    enum PlistKeys: String {
        case backgroundColor = "BACKGROUND_COLOR"
        case textColor = "TEXT_COLOR"
    }
    
    static var backgroundColor: UIColor {
        if let colorHexString = infoForKey(.backgroundColor) {
            return UIColor(hexString: colorHexString)
        }
        return .white
    }
    
    static var textColor: UIColor {
        if let colorHexString = infoForKey(.textColor) {
            return UIColor(hexString: colorHexString)
        }
        return .black
    }
    
    static func infoForKey(_ key: PlistKeys) -> String? {
        return Bundle.main.infoDictionary?[key.rawValue] as? String
    }
    
}
