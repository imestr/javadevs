//
//  UserTableViewCell.swift
//  JavaDevs
//
//  Created by Klemen Košir on 05/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var joinedDateLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: true)
    }
    
    func set(user: User?) {
        resetUI()
        if let user = user {
            nameLabel.text = user.name
            usernameLabel.text = user.username
            joinedDateLabel.text = "Joined: \(user.formattedCreatedAt)"
            avatarImageView.loadImage(url: user.avatarUrl)
            self.isUserInteractionEnabled = true
        }
        else {
            // should show placeholder
            let placeholderColor = UIColor(white: 0.97, alpha: 1.0)
            nameLabel.backgroundColor = placeholderColor
            usernameLabel.backgroundColor = placeholderColor
            joinedDateLabel.backgroundColor = placeholderColor
            avatarImageView.backgroundColor = placeholderColor
            self.isUserInteractionEnabled = false
        }
    }
    
    private func resetUI() {
        nameLabel.text = nil
        nameLabel.backgroundColor = .clear
        
        usernameLabel.text = nil
        usernameLabel.backgroundColor = .clear
        
        joinedDateLabel.text = nil
        joinedDateLabel.backgroundColor = .clear
        
        avatarImageView.image = nil
        avatarImageView.backgroundColor = UIColor(white: 0.97, alpha: 1.0)
        
        let textColor = Configuration.textColor
        nameLabel.textColor = textColor
        usernameLabel.textColor = textColor
        joinedDateLabel.textColor = textColor
    }

}
