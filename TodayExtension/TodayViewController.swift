//
//  TodayViewController.swift
//  TodayExtension
//
//  Created by Klemen Košir on 06/02/2019.
//  Copyright © 2019 Klemen Košir. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet private weak var infoLabel: UILabel!
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        if let loadedUsersNames = UserDefaults(suiteName: "group.klemenkosir.JavaDevs")?.array(forKey: "loadedUsers") as? [String] {
            let randomUserName = loadedUsersNames[Int.random(in: 0..<loadedUsersNames.count)]
            infoLabel.text = randomUserName
            completionHandler(NCUpdateResult.newData)
        }
        else {
            infoLabel.text = "-"
            completionHandler(NCUpdateResult.noData)
        }
    }
    
}
